---
output:
  pdf_document: default
  html_document: default
---
# Anglais Probabilités en jeux de hasard

Le plan peut se faire de plusieurs façons mais ce que je préfère c'est de partir d'un exemple, poser une question, parler d'un point de vue théorique (un peu en mode cours) et ensuite, on répond à la question avec cette théorie.

Par exemple, on parle du jeu avec les billes (schéma 1) sans le résultat en bas. La question est où est-ce que la bille a le plus de chance d'aller, on se rend compte que pour aller au centre il y a beaucoup plus de "chemin" que pour aller aux côtés, on explique que c'est une loi binomiale et qu'au final le résultat est donné par une loi normale. (on peut faire une petite preuve, c'est pas très compliqué et le prof de math sera content).
En fait, c'est littéralement le théorème central limite.
On peut ensuite faire le rapprochement avec une pièce qu'on lance un nombre n de fois et dire qu'au final, on sera toujours proche de 0.
Par contre si la pièce est truquée, ça change tout !
Imaginons que la pièce est 49% de tomber sur pile et 51% de tomber sur face, si on gagne quand on tombe sur pile et qu'on perd quand c'est face, dans ce cas, sur du long terme le joueur sera toujours perdant.
C'est dû à la simple raison que notre loi normale est centrée un peu dans le négatif et du coup, si on regarde notre intervalle de confiance pour un très grand nombre de lancers, on est sûr d'être dans le négatif.
Ca c'est à la fois une ouverture pour parler de l'espérance et pour parler de la loi des grands nombres, où en fait il y a des jeux qui paraissent "presque" équitables et qui pourtant sont perdants à 100%.
Là on peut rebomdir sur les jeux de casino qui ont tous des espérances de gains négatives pour le joueur.
Voilà, ça englobe pas mal de concepts donc en vrai je pense qu'on peut partir là dessus.

## Introduction 
On énoncerait le Théorème central limite en expliquant le lien avec les probas et les stats.
On utilise l'exemple de la Planche de Galton.
Poser une question: où est-ce que la bille a le plus de chance d'aller ?
Enoncer le plan.

https://sorciersdesalem.math.cnrs.fr/Galton/galton_plus.html

## Elements théoriques
TCL
Loi normale
Loi binomiale
Loi des grands nombres

## Théorème central limite avec une bille
Exemple du TCL avec la bille
Démonstration pour la convergence vers une distribution normale

## Une pièce de monnaie
Lois binomiales et lien avec le TCL
Distribution des lancers individuels à la distribution de leurs moyennes

## Les jeux au casino
Probabilité dans les jeux de hasard
TCL dans les Jeux de casino pour les résultats de jeux de casino et prendre un exemple de jeu

## Conclusion







