---
title: "Probability in Games of Chance"
author: "Amandine Liagre, Mattéo Tirole"
output: 
  powerpoint_presentation:
    keep_md: true
---

# Table of Contents

1. Introduction
2. Central Limit Theorem with a Ball
3. A Coin
4. Casino Games
5. Conclusion

# Introduction 


# Central limit theorem with a ball 

What is the probability that the marble is in the center? on the coast ?

Easy with the edges because there is only one path. (0 choices to make)
Result: 0.5 * 6 the ball at 6 intersections of probability 1/2.

In the middle:
You have to go as many times to the right as to the left. Out of 6 intersections, it's the same number to the right as to the left.
So we have 3 choices out of 6 to make. This gives us 20 possible paths.
So it's 20 times more likely than the sides.

$$\binom{6}{3} = \frac{6!}{3!(6 - 3)!} = \frac{6*5*4*3*2*1}{3*2*1*3*2*1} = \frac{6*5*4}{3*2} = 20$$

```{r, echo=FALSE}
  knitr::include_graphics('./schéma 1.png')
```

---

This result is due to the central limit theorem which says that: $\frac{S_n - n\mu}{\sigma \sqrt{n}} \xrightarrow[n\rightarrow \infty]{\mathscr{L}} \mathcal{N}(0,1)$

Where $S_n$ is the sum of all intersections which is the final result at the bottom (between -6 and 6). $\mu$ the mean because the ball has an equal chance of going left as right and $\sigma$ the variance which is 0.25.

Knowing that we have an average of zero, we have $S_n \xrightarrow[n\rightarrow \infty]{\mathscr{L}} \mathcal{N}(0,\sigma^2 n)$ by putting the variance inside.

In this way, if we simulate 100 balls, it gives this function:

```{r echo=FALSE, message=FALSE}
library(ggplot2)
set.seed(123)
x <- rnorm(100, mean = 0, sd = 0.25*6)
gains_moyen = 1:100
gains_moyen[1]=x[1]
for(i in 2:100){
  gains_moyen[i] = gains_moyen[i-1] + x[i]
}
for(i in 1:100){
  gains_moyen[i] = gains_moyen[i]/i
}
gains = data.frame(index = 1:100,gains_moyen, coup = x)
ggplot(gains)+
  aes(x = coup)+
  labs(x = "Result", y = "Sum", title = "Simulation of 100 balls")+
  geom_histogram()
```
Here the average gains are random but if we take many more balls for example 10000:


And in this case, it looks much more like a normal distribution.

```{r echo=FALSE, message=FALSE, warning=FALSE}
set.seed(123)
x <- rnorm(10000, mean = 0, sd = 0.25*6)
gains_moyen = 1:10000
gains_moyen[1]=x[1]
for(i in 2:10000){
  gains_moyen[i] = gains_moyen[i-1] + x[i]
}
for(i in 1:10000){
  gains_moyen[i] = gains_moyen[i]/i
}
gains = data.frame(index = 1:10000,gains_moyen, coup = x)
ggplot(gains)+
  aes(x = coup)+
  labs(x = "Result", y = "Sum", title = "Simulation of 10000 balls")+
  geom_histogram()
```

# A coin 

Now let's imagine this ball represents a coin and the game is to win a point if it lands on heads and lose a point if it lands on tails.
In this case, it is exactly the same scheme as before.
Furthermore, the law of large numbers tells us that if we play this game a very large number of times, then the average gain per turn will eventually be equal to the expectation of the game, which in this case is zero.

$$\frac{1}{n} \sum_{i=1}^n X_i \xrightarrow[n\rightarrow \infty]{\mathscr{L}} \mathbb{E}(X_1) = 0$$

Here is an example of average gains for 1000 dice rolls:

```{r echo=FALSE}
set.seed(123)
x <- rnorm(1000, mean = 0, sd = 0.25*6)
gains_moyen = 1:1000
gains_moyen[1]=x[1]
for(i in 2:1000){
  gains_moyen[i] = gains_moyen[i-1] + x[i]
}
for(i in 1:1000){
  gains_moyen[i] = gains_moyen[i]/i
}
gains = data.frame(index = 1:1000,gains_moyen, coup = x)
ggplot(data = gains)+
  aes(x = index, gains_moyen) +
  labs(x = "Number of rolls", y = "Average gain", title = "Average gain on a simulation of 100 rolls")+
  geom_line() + 
  geom_hline(yintercept = 0, col = "red")
```

---

We can see that at the beginning it's quite random but it quickly stabilizes at zero.

Now let's take a biased coin where the probability of landing on heads is 49% and tails is 51%.
In this case, the expectation is negative: 

$$\begin{align*}
\mathbb{E}(X) &= 1*\mathbb{P}(X = \text{tails}) - 1*\mathbb{P}(X = \text{heads}) \\
&= -0.51 + 0.49 \\
&= -0.02
\end{align*}$$




In this case, the law of large numbers tells us that our average gains are to lose 2 cents per roll.

$$ \frac{1}{n} \sum_{i=1}^n X_i \xrightarrow[n\rightarrow \infty]{\mathscr{L}} \mathbb{E}(X_1) = -0.02$$

This biased game allows over a large number of rolls, to be inevitably losing for the player.
After 10000 rolls, we see that we indeed end up at -0.02.

```{r echo=FALSE}
set.seed(12345)
x <- rnorm(10000, mean = -0.02, sd = (0.49^2)*6)
gains_moyen = 1:10000
gains_moyen[1]=x[1]
for(i in 2:10000){
  gains_moyen[i] = gains_moyen[i-1] + x[i]
}
for(i in 1:10000){
  gains_moyen[i] = gains_moyen[i]/i
}
gains = data.frame(index = 1:10000,gains_moyen, coup = x)
ggplot(data = gains)+
  aes(x = index, gains_moyen) +
  labs(x = "Number of rolls", y = "Average gain", title = "Average gain on a simulation of 10000 rolls")+
  geom_line()+
  geom_hline(yintercept = -0.02, col = "red")
```


# Casino games

All the games that a casino offers have a negative expectation which means that even if some players win on a limited number of rounds, the casino always ends up winning.

Here is a simple example: that of roulette.
You can bet on the color of the numbers (red, black, or green).
If you bet on red or black, you almost have a 50/50 chance of winning but not quite because of the green number where you lose half of the gains.

```{r, echo=FALSE}
   knitr::include_graphics('./roulette.webp')
```

---

There is one green number, 18 reds, and 18 blacks.
So here is the calculation of the expectation by always betting on red:

$$\begin{align*}
\mathbb{E}(X) &= -1*\mathbb{P}(X = \text{noir}) + 1*\mathbb{P}(X = \text{rouge}) - \frac{1}{2}\mathbb{P}(X = \text{vert}) \\
&= -\frac{18}{37} + \frac{18}{37} - \frac{1}{2}*\frac{1}{37} \\
&= -0.013
\end{align*}$$


In the long run, a player loses on average 1.3 cents per spin.


# Conclusion


